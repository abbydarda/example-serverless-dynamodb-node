require('dotenv').config()
const db = require('./db')
const {
    ScanCommand,
    GetItemCommand,
    PutItemCommand,
    UpdateItemCommand,
    DeleteItemCommand,
} = require('@aws-sdk/client-dynamodb')
const { marshall, unmarshall } = require('@aws-sdk/util-dynamodb')

const getPost = async (event) => {
    const response = { statusCode: 200 }

    try {
        const params = {
            TableName: process.env.DYNAMODB_POST_TABLE,
            Key: marshall({ postId: event.pathParameters.postId })
        }

        const { Item } = await db.send(new GetItemCommand(params))
        response.body = JSON.stringify(
            {
                message: "successfully to get data post.",
                data: (Item) ? unmarshall(Item) : {},
                rawData: Item
            }
        )

        return response
    } catch (e) {
        console.log(e);
        response.statusCode = 500
        response.body = {
            message: "Failed to get data post.",
            errorMsg: e.message,
            errorStack: e.stack,
        }

        return response
    }
}

const createPost = async (event) => {
    const response = { statusCode: 200 }

    try {
        const body = JSON.parse(event.body)
        const params = {
            TableName: process.env.DYNAMODB_POST_TABLE,
            Item: marshall(body || {})
        }
        console.log(params);

        const createPost = await db.send(new PutItemCommand(params))
        response.body = JSON.stringify(
            {
                message: "successfully to create data post.",
                createPost
            }
        )

        return response
    } catch (e) {
        console.log(e);
        response.statusCode = 500
        response.body = {
            message: "Failed to create data post.",
            errorMsg: e.message,
            errorStack: e.stack,
        }

        return response
    }
}

const updatePost = async (event) => {
    const response = { statusCode: 200 }

    try {
        const body = JSON.parse(event.body)
        const objKeys = Object.keys(body)

        const params = {
            TableName: process.env.DYNAMODB_POST_TABLE,
            Key: marshall({ postId: event.pathParameters.postId }),
            UpdateExpression: `SET ${objKeys.map((_, index) => `#key${index} = :value${index}`).join(",", "")}`,
            ExpressionAttributeNames: objKeys.reduce((acc, key, index) => ({
                ...acc,
                [`#key${index}`]: key,
            }), {}),
            ExpressionAttributeValues: marshall(objKeys.reduce((acc, key, index) => ({
                ...acc,
                [`:value${index}`]: body[key],
            }), {}))
        }


        const updatePost = await db.send(new UpdateItemCommand(params))
        response.body = JSON.stringify(
            {
                message: "successfully to update data post.",
                updatePost
            }
        )

        return response
    } catch (e) {
        console.log(e);
        response.statusCode = 500
        response.body = {
            message: "Failed to update data post.",
            errorMsg: e.message,
            errorStack: e.stack,
        }

        return response
    }
}

const deletePost = async (event) => {
    const response = { statusCode: 200 }

    try {
        const params = {
            TableName: process.env.DYNAMODB_POST_TABLE,
            Key: marshall({ postId: event.pathParameters.postId }),
        }

        const deletePost = await db.send(new DeleteItemCommand(params))
        response.body = JSON.stringify(
            {
                message: "successfully to delete data post.",
                deletePost
            }
        )

        return response
    } catch (e) {
        console.log(e);
        response.statusCode = 500
        response.body = {
            message: "Failed to delete data post.",
            errorMsg: e.message,
            errorStack: e.stack,
        }

        return response
    }
}

const getAllPost = async (event) => {
    const response = { statusCode: 200 }

    try {
        const { Items } = await db.send(new ScanCommand({ TableName: process.env.DYNAMODB_POST_TABLE }))

        response.body = JSON.stringify(
            {
                message: "successfully to retrieve data post.",
                data: Items.map(item => unmarshall(item)),
                Items,
            }
        )

        return response
    } catch (e) {
        console.log(e);
        response.statusCode = 500
        response.body = {
            message: "Failed to retrieve data post.",
            errorMsg: e.message,
            errorStack: e.stack,
        }

        return response
    }
}

module.exports = {
    getPost,
    createPost,
    updatePost,
    deletePost,
    getAllPost
}
