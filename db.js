const { DynamoDBClient } = require('@aws-sdk/client-dynamodb')
const client = new DynamoDBClient(
    {
        region: "ap-southeast-1",
        accessKeyId: "access_key_id",
        secretKeyAccessId: "secret_access_key_id",
        endpoint: "http://localhost:8001"
    }
)

module.exports = client

