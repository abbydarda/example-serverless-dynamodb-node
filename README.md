# NodeJS | Serverless Offline | Dynamodb Local

## Version
| Nama | Versi |
| ----------- | ----------- |
| Node | ```v16.13.1``` |
| npm | ```v8.3.0``` |
| serverless | `- Framework Core: 2.70.0 (local)`<br>  `- Plugin: 5.5.3<br> SDK: 4.3.0`<br> `- Components: 3.18.1` |
| java | `- openjdk 11.0.3 2019-04-16`<br> `- OpenJDK Runtime Environment (build 11.0.3+7-Ubuntu-1ubuntu218.10.1)` <br> `- OpenJDK 64-Bit Server VM (build 11.0.3+7-Ubuntu-1ubuntu218.10.1, mixed mode, sharing)` |



## Package
| Nama | Link |
| ----------- | ----------- |
| Serverless | [serverless](https://www.npmjs.com/package/serverless) |
| Serverless Offline | [serverless Offline](https://www.npmjs.com/package/serverless-offline) |
| Serverless Iam Role per Function | [Serverless Iam Role per Function](https://www.npmjs.com/package/serverless-iam-roles-per-function) |
| Serverless Dynamodb Local | [Serverless Dynamodb Local](https://github.com/99x/serverless-dynamodb-local) |
| AWS SDK for JavaScript v3 | [AWS SDK for JavaScript v3](https://github.com/aws/aws-sdk-js-v3) |
| AWS SDK Util Dynamodb | [AWS SDK Util Dynamodb](https://www.npmjs.com/package/@aws-sdk/util-dynamodb) |
| Dotenv | [Dotenv](https://www.npmjs.com/package/dotenv) |

## Installation
```bash
npm install
```

```bash
sls dynamodb install
```

```bash
sls offline start
```

## Endpoint
### `[GET] /dev/post`
### Response
```json
{
    "message": "successfully to retrieve data post.",
    "data": [],
    "Items": []
}
```
```json
{
    "message": "successfully to retrieve data post.",
    "data": [
        {
            "name": "hello world!!!",
            "description": "ini description!!!",
            "postId": "1"
        }
    ],
    "Items": [
        {
            "name": {
                "S": "hello world!!!"
            },
            "description": {
                "S": "ini description!!!"
            },
            "postId": {
                "S": "1"
            }
        }
    ]
}
```

### `[GET] /dev/post/1`
### Response
```json
{
    "message": "successfully to get data post.",
    "data": {}
}
```
```json
{
    "message": "successfully to get data post.",
    "data": {
        "name": "hello world!!!",
        "description": "ini description!!!",
        "postId": "1"
    },
    "rawData": {
        "name": {
            "S": "hello world!!!"
        },
        "description": {
            "S": "ini description!!!"
        },
        "postId": {
            "S": "1"
        }
    }
}
```

### `[POST] /dev/post`
### Payload
```json
{
    "postId" : "1",
    "name" : "hello world!!!",
    "description" : "ini description!!!"
}
```
### Response
```json
{
    "message": "successfully to create data post.",
    "createPost": {
        "$metadata": {
            "httpStatusCode": 200,
            "requestId": "de5d3649-bb7b-465c-882d-f6ddc338ec02",
            "attempts": 1,
            "totalRetryDelay": 0
        }
    }
}
```

### `[PUT] /dev/post/1`
### Payload
```json
{
    "name" : "hello world",
    "description" : "ini description"
}
```
### Response
```json
{
    "message": "successfully to update data post.",
    "updatePost": {
        "$metadata": {
            "httpStatusCode": 200,
            "requestId": "a09d3681-0b50-4e21-aef1-fe57b2b5c5ef",
            "attempts": 1,
            "totalRetryDelay": 0
        }
    }
}
```

### `[DELETE] /dev/post/1`

### Response
```json
{
    "message": "successfully to delete data post.",
    "deletePost": {
        "$metadata": {
            "httpStatusCode": 200,
            "requestId": "82da8551-c10e-4af7-ab48-751e0d557478",
            "attempts": 1,
            "totalRetryDelay": 0
        }
    }
}
```





